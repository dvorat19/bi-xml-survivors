<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version = '2.0' 
     xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

	<xsl:template match="/" >
		<xsl:result-document href="./output/html/Index.html">
			<html lang="en">
				<head>
                    <title>Index</title>
				</head>
				<body>
                    <h1>Index</h1>
					<ul>
						<xsl:apply-templates select="/project/country" mode="index"/>
					</ul>
				</body>
			</html>
		</xsl:result-document>
		
		<xsl:apply-templates select="/project/country" mode="page" />
	</xsl:template>
	
	<xsl:template match="/project/country" mode="index" >
		<li>
			<a href="./{@name}.html"><xsl:value-of select="@name"/> link</a>
		</li>
	</xsl:template>

	<xsl:template match="/project/country" mode="page" >
		<xsl:result-document href="./output/html/{@name}.html">
			<html lang="en">
				<head>
                    <title>
						<xsl:value-of select="@name" />
					</title>
				</head>
				<body>
                    <h1>Stranka pro <xsl:value-of select="@name" /></h1>
				</body>
			</html>
		</xsl:result-document>
	</xsl:template>
	
</xsl:stylesheet>

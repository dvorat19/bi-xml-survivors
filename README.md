# BI-XML-Survivors

BI-XML semestrální práce


# Parser
Příklad spustí parser na URL s danou zemí
```java -jar parser/parser.jar URL```

# Vytvoreni source
Zavolame script prikazem 
```./generate_source.sh```

Script provede pouze prikaz:
```xmllint --noent source.xml > generated_source.xml```
Nazvy souboru NESMI obsahovat mezery.
Je mozne ze po parsovani je potreba odstranit **standalone="no"** ze zacatku souboru statu.

# Spuštění validace

## DTD
```xmllint --noout --dtdvalid validation/validation.dtd generated_source.xml```

## RelaxNG
### Pro všechny
```
jing validation/validation_all.rng source.xml
jing -c validation/validation_all.rnc source.xml
```
 
### Pro jeden
```
jing validation/validation_one.rng data/Yemen.xml
jing -c validation/validation_one.rnc data/Yemen.xml
```

# XSLT

Ve složce xslt pustit script transform. Jestli je nainstalován Firefox, tak stránka se spustí automaticky. Jinak je nutné ve složce ručně otevřít soubor index.HTML

# PDF
```
java -jar saxon/saxon9he.jar generated_source.xml pdf.xslt > pdf.fo 
fop -fo pdf.fo -pdf countries.pdf 2>/dev/null
```

# Prezentace
```
https://docs.google.com/presentation/d/1AIWKNRYeAU898ZbYiRakEcVqjpmMHYk9nqfEWp2BRkA/edit?usp=sharing
```

<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="A4-portrait" page-height="29.7cm" page-width="21.0cm" margin="2cm"
                                       margin-bottom="1cm"
									   margin-top="1cm">
                    <fo:region-body margin-top="1cm" margin-bottom="2cm"/>
                    <fo:region-before extent="1cm"/>
                    <fo:region-after extent="1cm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:declarations>
                <x:xmpmeta xmlns:x="adobe:ns:meta/">
                    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                        <rdf:Description rdf:about="" xmlns:dc="http://purl.org/dc/elements/1.1/">
                            <dc:title>World Factbook</dc:title>
                        </rdf:Description>
                    </rdf:RDF>
                </x:xmpmeta>
            </fo:declarations>
            <fo:page-sequence master-reference="A4-portrait">
				
				<fo:static-content flow-name="xsl-region-before">
					<fo:block font-size="10pt" font-weight="400" text-align="right">
						<fo:basic-link internal-destination="info" color="blue" text-decoration="underline">
							Available countries
						</fo:basic-link>
                    </fo:block>
				</fo:static-content>
				
				 <fo:static-content flow-name="xsl-region-after">
                    <fo:block font-size="10pt" font-weight="400" text-align="center">
                        Page
                        <fo:page-number/>
                    </fo:block>
                </fo:static-content>

                <fo:flow flow-name="xsl-region-body">
                    <fo:block font-size="18pt" font-weight="400" text-align="center" id="info">
                        World Factbook
                    </fo:block>
                    <xsl:apply-templates/>
                </fo:flow>

            </fo:page-sequence>

        </fo:root>
    </xsl:template>

    <xsl:template match="project/countries">
        <fo:block>
            Available countries:
        </fo:block>
        <fo:list-block provisional-distance-between-starts="0.3cm" provisional-label-separation="0.15cm">
            <xsl:for-each select="country">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block>
                            <fo:inline>&#183;</fo:inline>
                        </fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
							<fo:basic-link internal-destination="{@name}" color="blue" text-decoration="underline">
                                <xsl:value-of select="@name"/>
							</fo:basic-link>
                            

                            <fo:list-block provisional-distance-between-starts="0.3cm"
                                           provisional-label-separation="0.15cm"
                                           margin-left="15pt">

                                <fo:list-item>
                                        <fo:list-item-label end-indent="label-end()">
                                            <fo:block>
                                                <fo:inline>&#183;</fo:inline>
                                            </fo:block>
                                        </fo:list-item-label>
                                        <fo:list-item-body start-indent="body-start()">
                                            <fo:block>
                                                <fo:basic-link internal-destination="{generate-id(@name)}" color="blue"
                                                               text-decoration="underline">
                                                    Pictures
                                                </fo:basic-link>
                                            </fo:block>
                                        </fo:list-item-body>
                                    </fo:list-item>

                                <xsl:for-each select="section">
                                    <fo:list-item>
                                        <fo:list-item-label end-indent="label-end()">
                                            <fo:block>
                                                <fo:inline>&#183;</fo:inline>
                                            </fo:block>
                                        </fo:list-item-label>
                                        <fo:list-item-body start-indent="body-start()">
                                            <fo:block>
                                                <fo:basic-link internal-destination="{generate-id(.)}" color="blue"
                                                               text-decoration="underline">   
                                                    <xsl:value-of select="concat(upper-case(substring(@name,1,1)),
														  substring(@name, 2),
														  ' '[not(last())]
														 )
												  "/>
                                                </fo:basic-link>
                                            </fo:block>
                                        </fo:list-item-body>
                                    </fo:list-item>
                                </xsl:for-each>
                            </fo:list-block>

                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
        </fo:list-block>
        <xsl:apply-templates select="country"/>
    </xsl:template>

    <xsl:template match="project/countries/country">
        <fo:block page-break-before="always"/>
        <fo:block font-size="15pt" font-weight="400" text-align="center" id="{@name}" padding-top="15pt">
                <xsl:value-of select="@name"/>
        </fo:block>
		<fo:block text-align="center" padding-top="5pt">
		 <fo:external-graphic height="20px"
                                     content-width="scale-to-fit"
                                     content-height="scale-to-fit"
                                     scaling="uniform">
                    <xsl:attribute name="src">
                        url('./xslt/output/images/<xsl:value-of select="@name"/>/flag.gif')
                    </xsl:attribute>
                </fo:external-graphic>
        </fo:block>

        <fo:block font-size="15pt" font-weight="400" text-align="center" margin-top="30pt" id="{generate-id(@name)}">
            Pictures
        </fo:block>
		
		<fo:block text-align="center" padding-top="5pt" padding-bottom="5pt">
		 <fo:external-graphic height="200px"
                                     content-width="scale-to-fit"
                                     content-height="scale-to-fit"
                                     scaling="uniform">
                    <xsl:attribute name="src">
                        url('./xslt/output/images/<xsl:value-of select="@name"/>/large.jpg')
                    </xsl:attribute>
         </fo:external-graphic>
        </fo:block>
		
		<fo:block text-align="center" padding-top="5pt" padding-bottom="5pt">
		 <fo:external-graphic height="200px"
                                     content-width="scale-to-fit"
                                     content-height="scale-to-fit"
                                     scaling="uniform">
                    <xsl:attribute name="src">
                        url('./xslt/output/images/<xsl:value-of select="@name"/>/map.gif')
                    </xsl:attribute>
            </fo:external-graphic>
        </fo:block>
		
        <xsl:apply-templates select="section"/>
    </xsl:template>

    
    <xsl:template match="section">
        <fo:block font-size="15pt" font-weight="400" text-align="center" margin-top="30pt" id="{generate-id(.)}">
		<xsl:value-of select="concat(upper-case(substring(@name,1,1)),
														  substring(@name, 2)
														 )
												  "/>
        </fo:block>

        <xsl:apply-templates select="text"/>
    </xsl:template>

    <xsl:template match="section/text ">
        <fo:block margin-top="5pt">
            <fo:inline font-weight="400">
                <xsl:value-of select="concat(upper-case(substring(@name,1,1)),
														  substring(@name, 2)
														 )
												  "/>
            </fo:inline>
            :
            <fo:inline margin-left="15pt">
                <xsl:value-of select="."/>
            </fo:inline>
        </fo:block>
    </xsl:template>



</xsl:stylesheet>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xls="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">

        <html>
            <head>
                <xsl:apply-templates mode="title"></xsl:apply-templates>
                <!--Styleas and js-->
                <link rel="stylesheet" href="styles.css"/>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"/>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
            </head>
            <body>
                <header>
                    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
                        <xsl:apply-templates mode="menu"></xsl:apply-templates>
                    </nav>
                </header>
                <div class="content container">
                    <xsl:apply-templates mode="content"></xsl:apply-templates>
                </div>
            </body>
        </html>

    </xsl:template>

    <xsl:template match="country" mode="title">
        <title>
            <xsl:value-of select="@name"></xsl:value-of> | Webpage
        </title>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
    </xsl:template>

    <xsl:template match="country" mode="menu">
        <ul class="nav nav-pills nav-dark">
            <li><a data-toggle="pill" href="#home" class="nav-item nav-link active">Home</a></li>
            <xsl:for-each select="section">
                <li>
                    <a data-toggle="pill" href='#menu{position()}' class="nav-item nav-link">
                            <xsl:value-of select="@name"></xsl:value-of>
                    </a>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>
    <xsl:template match="country" mode="content">

        <div class="tab-content">
            <div id="home" class="tab-pane fade show active">
                <h2 class="text-center" style="margin-top:20px;">
                    <xsl:value-of select="@name"></xsl:value-of>
                </h2>
                <div class="container">
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <div class="col-6">
                                    <img src="images/{attribute::name}/flag.gif" alt="{attribute::name}"/>
                                </div>
                                <div class="col-6">
                                    <img src="images/{attribute::name}/map.gif" alt="{attribute::name}"/>
                                </div>
                            </div>
                            <div class="text-center">
                                <a href="https://www.google.com/search?q={attribute::name}">See more on Google</a>
                            </div>
                        </div>
                        <div class="col-6">
                            <img src="images/{attribute::name}/large.jpg" alt="{attribute::name}"/>
                        </div>
                    </div>
                </div>

            </div>
            <xls:for-each select="section">
                <div id='menu{position()}' class="tab-pane fade">
                    <h2 class="text-center">
                        <xsl:value-of select="@name"></xsl:value-of>
                    </h2>
                    <xsl:for-each select="text">
                        <h3>
                            <xsl:value-of select="@name"></xsl:value-of>
                        </h3>
                        <xsl:apply-templates select="data"></xsl:apply-templates>
                    </xsl:for-each>
                </div>
            </xls:for-each>
        </div>
    </xsl:template>
    <xsl:template match="data">

        <xsl:choose>
            <xsl:when test="subdata">
                <xsl:for-each select="subdata">
                    <p>
                        <xsl:value-of select="text()"></xsl:value-of>
                    </p>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <p>
                    <xsl:value-of select="text()"></xsl:value-of>
                </p>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>
</xsl:stylesheet>